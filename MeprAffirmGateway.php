<?php
if (!defined('ABSPATH')) {
  die('You are not allowed to call this page directly.');
}


class MeprAffirmGateway extends MeprBaseRealGateway 
{
    /** This will be where the gateway api will interacted from */
    public $affirm_api;

    /** Used in the view to identify the gateway */
    public function __construct()
    {
        $this->name = __("Affirm", 'memberpress');
        $this->desc = __('Pay via Affirm', 'memberpress');
        $this->has_spc_form = true;
        $this->set_defaults();

        $this->capabilities = array(
        'process-payments',
        'process-refunds',
        'create-subscriptions',
        'cancel-subscriptions',
        'suspend-subscriptions',
        'resume-subscriptions',
        );

        // Setup the notification actions for this gateway
        $this->notifiers = array(
            'confirmation' => 'webhook_confirmation'
        );

        $this->message_pages = array(
            'payment_failed' => 'payment_failed_message' 
        );

    }

    /** The system uses this to load this if there's a payment option configured for this */
    public function load($settings)
    {
      $this->settings = (object) $settings;
      $this->set_defaults();
      $this->affirm_api = new MeprAffirmAPI($this);
    }

    /** Sets the defaults for the settings, etc */
    protected function set_defaults()
    {
        if (!isset($this->settings)) {
            $this->settings = array();
        }
    
        $this->settings = (object) array_merge(
            array(
            'gateway' => 'MeprAffirmGateway',
            'id' => $this->generate_id(),
            'label' => '',
            'use_label' => true,
            'icon' => MP_AFFIRM_IMAGES_URL . '/logo.svg',
            'use_icon' => true,
            'use_desc' => true,
            'debug' => $this->is_test_mode(),
            'email' => '',
            'test_mode' => false,
            'api_keys' => array(
                'test' => array(
                    'public' => '',
                    'secret' => ''
                ),
                'live' => array(
                    'public' => '',
                    'secret' => ''
                )
            )
            ),
            (array) $this->settings
        );
    
        $this->id = $this->settings->id;
        $this->label = $this->settings->label;
        $this->use_label = $this->settings->use_label;
        $this->use_icon = $this->settings->use_icon;
        $this->use_desc = $this->settings->use_desc;
    
        if ($this->is_test_mode()) {
            $this->settings->public_key = trim($this->settings->api_keys['test']['public']);
            $this->settings->secret_key = trim($this->settings->api_keys['test']['secret']);
        } else {
            $this->settings->public_key = trim($this->settings->api_keys['live']['public']);
            $this->settings->secret_key = trim($this->settings->api_keys['live']['secret']);
        }
    }

    /** Affirm confirmation pay
     * 
     */
    public function webhook_confirmation()
    {
        $mepr_options = MeprOptions::fetch();
        try{
            $checkout_token = isset( $_POST['checkout_token'] ) ?  $_POST['checkout_token']  : '';
            $transaction_id = isset($_GET['transaction_id']) ? $_GET['transaction_id'] : '';
            $billing_type = isset($_GET['billing_type']) ? $_GET['billing_type'] : '';

            if ( empty( $checkout_token ) ) {
                throw new Exception(
                    'Checkout failed. No token was provided by Affirm. You may wish to try a different payment method.'
                );
            }

            if(empty( $transaction_id)){
                throw new Exception(
                    'Checkout failed. No order id. Please try again.'
                );
            } 
            $transaction = MeprTransaction::get_one_by_trans_num($transaction_id);
            $transaction = new MeprTransaction($transaction->id); 
                        
            $result = $this->affirm_api->request_charge_id_for_token($checkout_token);
            if(is_wp_error($result)){
                throw new Exception(
					__( 'Checkout failed. Order mismatch for Affirm token. Please try checking out again later, or try a different payment source.', 'woocommerce-gateway-affirm' )
				);
            }
            
            $charge_id = $result['charge_id'];
            $transaction->trans_num =  $charge_id;
            $transaction->store();
            
            if ( ! $this->affirm_api->capture_charge( $charge_id, $transaction->trans_num) ) {
                $this->affirm_api->void_charge( $charge_id);
                throw new Exception(
					__( 'Checkout failed. Order mismatch for Affirm token. Please try checking out again later, or try a different payment source.', 'woocommerce-gateway-affirm' )
				);
            }
        
            $_REQUEST['x_trans']  = $transaction;
            $_REQUEST['status']    = true;
            
            if($billing_type == 'one_time')
            {
                $this->record_payment();
            }
            else if($billing_type == 'reccuring')
            {
                $this->record_create_subscription();
            }else{
                throw new Exception(
					__( 'Unknown billing type.', 'memberpress-gateway-affirm' )
				);
            }

            $txn = new MeprTransaction($transaction->id);
            $product = new MeprProduct($txn->product_id);
            $sanitized_title = sanitize_title($product->post_title);
            $query_params = array('membership' => $sanitized_title, 'trans_num' => $txn->trans_num, 'membership_id' => $product->ID);
            MeprUtils::wp_redirect($mepr_options->thankyou_page_url(build_query($query_params)));
        
        }catch(Exception $e){
            if((isset($transaction) || isset($transaction_id)) && isset($billing_type))
            {
                $transaction = isset($transaction) ? new MeprTransaction($transaction->id) : new MeprTransaction(MeprTransaction::get_one_by_trans_num($transaction_id)->id);
                $product = new MeprProduct($transaction->product_id);
                $_REQUEST['x_trans'] = $transaction;
                $_REQUEST['status'] = false;

                if($billing_type == 'one_time'){
                    $this->record_payment_failure();
                }else if($billing_type == 'reccuring')
                {
                    $this->record_create_subscription();
                }
             
               MeprUtils::wp_redirect(add_query_arg(
                    array(
                        'error' => $e->getMessage(),
                    ), 
                    $this->message_page_url($product, 'payment_failed')
               ));
            }
            
            MeprUtils::wp_redirect(home_url());
        }
    }


    public function process_payment_form($txn) 
    {
        $user = $txn->user();
        $product = $txn->product();

        $first_name = sanitize_text_field(wp_unslash(($_POST['user_first_name'])));
        $last_name = sanitize_text_field(wp_unslash(($_POST['user_last_name'])));
    
        if(empty($user->first_name)) {
          update_user_meta($user->ID, 'first_name', $first_name);
        }
    
        if(empty($user->last_name)) {
          update_user_meta($user->ID, 'last_name', $last_name);
        }
    
        if( !$product->is_one_time_payment() && (isset($_REQUEST['mepr_cc_num']) || !empty($_REQUEST['mepr_cc_num'])) )
        {
            $card_num = sanitize_text_field(wp_unslash(($_REQUEST['mepr_cc_num'])));
            $exp = sanitize_text_field(wp_unslash(($_REQUEST['mepr_cc_exp'])));
            $cvc = sanitize_text_field(wp_unslash(($_REQUEST['mepr_cvv_code'])));

            update_user_meta($user->ID, 'mepr_cc_num', $card_num);
            update_user_meta($user->ID, 'mepr_cc_exp', $exp);
            update_user_meta($user->ID, 'mepr_cvv_code', $cvc);
        }

        parent::process_payment_form($txn);
      }

    
    /** Used to send data to a given payment gateway. In gateways which redirect
    * before this step is necessary this method should just be left blank.
    */
    public function process_payment($transaction)
    {
        $billing_type = isset($_POST['mepr_billing_type']) ? $_POST['mepr_billing_type'] : 'one_time';
        $result = $this->prepare_to_pay($transaction, $billing_type);      
        if ( is_wp_error( $result ) ) {
            throw new Exception(
                __( $result->get_error_message(), 'woocommerce-gateway-affirm' )
            );
        }
        MeprUtils::wp_redirect($result['redirect_url']);
    }

    public function prepare_to_pay($transaction, $billing_type)
    {
        global $post;

        if(isset($transaction) and $transaction instanceof MeprTransaction) {
            $user = $transaction->user();
            $product = $transaction->product();
        }
        else
            throw new MeprGatewayException( __('Payment was unsuccessful, please check your payment details and try again.', 'memberpress') );
      
        if(!$product->is_one_time_payment() && $product->period_type != 'years')
            throw new MeprGatewayException( __('Payment was unsuccessful. Affirm for years subscriptions only.', 'memberpress') );


        $mepr_options = MeprOptions::fetch();
        $current_post = MeprUtils::get_current_post();
        $item_image_id = $post ?  $post->get_image_id() : '';
        $image_attributes = wp_get_attachment_image_src( $item_image_id );
        if ( is_array( $image_attributes ) ) {
            $item_image_url = $image_attributes[0];
        }

        $redirect_url = add_query_arg(
			array(
				'transaction_id' => $transaction->trans_num,
                'billing_type' => $billing_type 
			), $this->notify_url('confirmation')
		);
   
        $args = array(
            'merchant' => array(
                'public_api_key' => $this->settings->public_key,
                'user_cancel_url' => $redirect_url,
                'user_confirmation_url' => $redirect_url,
            ),
            'billing' => array(
                'name' => array(
                    'full' => $user->get_full_name(),
                ),
                'address' => array(
                    'line1' => $_POST['mepr_adress'],
                    'city' => $_POST['mepr_city'],
                    'state' => $_POST['mepr_state'],
                    "zipcode" => $_POST['mepr_zip_post_code'],
                ),
            ),
            'shipping' => array(
                'name' => array(
                    'full' => $user->get_full_name(),
                ),
                'address' => array(
                    'line1' => $_POST['mepr_adress'],
                    'city' => $_POST['mepr_city'],
                    'state' => $_POST['mepr_state'],
                    "zipcode" => "94107",
                ),
            ),
            'items' => array(
                array(
                    'display_name' => $product->post_title,
                    'sku' => $transaction->product_id,
                    'unit_price' => (int) MeprUtils::format_float($product->price * 100, 0),
                    'qty' => 1,
                    'item_image_url' => isset( $item_image_url ) ?  $item_image_url : '',
                    'item_url' => $product->access_url,
                ),
            ),
            'currency' => $mepr_options->currency_code ,
            'shipping_amount' => 0.00,
            'order_id' => $transaction->trans_num,
            'tax_amount' => (int) MeprUtils::format_float($transaction->tax_amount * 100, 0),
            'total' =>(int) MeprUtils::format_float( $transaction->total * 100, 0)
      );

      if(isset($_POST['mepr_user_phone'])){
        $args['billing']['phone_number'] = $_POST['mepr_user_phone'];
      }

      if(isset($_POST['mepr_user_email'])){
        $args['billing']['email'] = $_POST['mepr_user_email'];
      }

      else if(isset($_POST['user_email'])){
        $args['billing']['email'] = $_POST['user_email'];
      }

      $res = $this->affirm_api->request_checkout_store($args);
      return $res;
    }


    public function payment_failed_message()
    {
        $mepr_options = MeprOptions::fetch();
        ?>
          <h4><?php _e('Your payment at Affirm failed.', 'memberpress'); ?></h4>
          <p><?php _e(isset($_REQUEST['error']) ? $_REQUEST['error'] : 'Payment failed. Please try again.', 'memberpress')?></p>
        <?php
    }

    /** Used to record a successful payment by the given gateway. It should have
        * the ability to record a successful payment or a failure. It is this method
        * that should be used when receiving an IPN from PayPal or a Silent Post
        * from Authorize.net.
        */
    public function record_payment()
    {
        if(!isset($_REQUEST['x_trans']) or !isset($_REQUEST['status']))
            return false;

        $res = $_REQUEST['status'];
        $txn = $_REQUEST['x_trans'];

        if( $txn->status == MeprTransaction::$complete_str )
            return false;


        if($res == true){
            $txn->txn_type   = MeprTransaction::$payment_str;
            $txn->status     = MeprTransaction::$complete_str;

            $upgrade = $txn->is_upgrade();
            $downgrade = $txn->is_downgrade();
    
            $event_txn = $txn->maybe_cancel_old_sub();
            $txn->store();

            $this->email_status("Transaction\n" . MeprUtils::object_to_string($txn->rec, true) . "\n", $this->is_test_mode());

            $prd = $txn->product();
    
            if( $prd->period_type=='lifetime' ) {
              if( $upgrade ) {
                $this->upgraded_sub($txn, $event_txn);
              }
              else if( $downgrade ) {
                $this->downgraded_sub($txn, $event_txn);
              }
              else {
                $this->new_sub($txn);
              }
    
              MeprUtils::send_signup_notices( $txn );
            }
    
            MeprUtils::send_transaction_receipt_notices( $txn );
            return $txn;
        }

        return false;
    }

    /** This method should be used by the class to push a request to to the gateway.
        */
    public function process_refund(MeprTransaction $txn)
    {
        $mepr_options = MeprOptions::fetch();
        $args = MeprHooks::apply_filters('mepr_affirm_ec_refund_args', array(
            'TRANSACTIONID' => $txn->trans_num,
            'REFUNDTYPE' => 'Full',
            'CURRENCYCODE' => $mepr_options->currency_code
          ), $txn);
        $this->email_status("RefundTransaction Request:\n".MeprUtils::object_to_string($args,true)."\n", $this->is_test_mode());
        $result = $this->affirm_api->refund_charge($txn->trans_num);

        if ( is_wp_error( $result ) || !isset($result['id']) ) {
            throw new MeprGatewayException( __('The refund was unsuccessful. Please login at Affirm and refund the transaction there.', 'memberpress') );
        }

        $_POST['txn_id'] = $txn->trans_num;
        return $this->record_refund();
    }

    /** This method should be used by the class to record a successful refund from
        * the gateway. This method should also be used by any IPN requests or Silent Posts.
        */
    public function record_refund()
    {
        $obj = MeprTransaction::get_one_by_trans_num($_POST['txn_id']);
        if(!is_null($obj) && (int)$obj->id > 0)
        {
          $txn = new MeprTransaction($obj->id);
    
          // Seriously ... if txn was already refunded what are we doing here?
          if($txn->status == MeprTransaction::$refunded_str) { return $txn; }
    
          $txn->status = MeprTransaction::$refunded_str;
    
          $this->email_status("Processing Refund: \n" . MeprUtils::object_to_string($_POST) . "\n Affected Transaction: \n" . MeprUtils::object_to_string($txn), $this->is_test_mode());
    
          $txn->store();
    
          MeprUtils::send_refunded_txn_notices($txn);
    
          return $txn;
        }
    
        return false;
    }

    /** Used to record a successful recurring payment by the given gateway. It
        * should have the ability to record a successful payment or a failure. It is
        * this method that should be used when receiving an IPN from PayPal or a
        * Silent Post from Authorize.net.
        */
    public function record_subscription_payment()
    {
        // Nothing here yet
    }

    /** Used to record a declined payment. */
    public function record_payment_failure()
    {   
        if(!isset($_REQUEST['x_trans']))
            return false;

        $txn = $_REQUEST['x_trans'];
        $txn->status = MeprTransaction::$failed_str;
        $txn->store();
        MeprUtils::send_failed_txn_notices($txn);
        return $txn;
    }

    /** Used to process a one-off payment for a trial period.
        * Should be used for gateways that don't support trial periods
        * on recurring subscriptions, or for gateways that don't
        * support flexible trial periods/amounts but do support
        * setting the subscription start date to some time in the future.
        * Authorize.net and Stripe.com currently use this method.
        */
    public function process_trial_payment($transaction)
    {
        $mepr_options = MeprOptions::fetch();
        $sub = $transaction->subscription();
    
        $transaction->set_subtotal($sub->trial_amount);
        $transaction->status = MeprTransaction::$pending_str;
    
        //Attempt processing the payment here - the send_aim_request will throw the exceptions for us
        $this->process_create_subscription($transaction);    
    }

    /** See above -- process_trial_payment() method
        */
    public function record_trial_payment($transaction)
    {
        //Nothing (record will be in record_create_subscription)
    }

    /** Used to send subscription data to a given payment gateway. In gateways
        * which redirect before this step is necessary this method should just be
        * left blank.   
        */
    public function process_create_subscription($transaction)
    {
        $result = $this->prepare_to_pay($transaction, 'reccuring');      
        if ( is_wp_error( $result ) ) {
            throw new Exception(
                __( $result->get_error_message(), 'woocommerce-gateway-affirm' )           
            );
        }
        MeprUtils::wp_redirect($result['redirect_url']);
    }

    /** Used to record a successful subscription by the given gateway. It should have
        * the ability to record a successful subscription or a failure. It is this method
        * that should be used when receiving an IPN from PayPal or a Silent Post
        * from Authorize.net.
        */
    public function record_create_subscription()
    {
        $mepr_options = MeprOptions::fetch();
           
        if(!isset($_REQUEST['x_trans']))
            return false;

        $status = $_REQUEST['status'];
        $auth_transaction = $_REQUEST['x_trans'];

        if($status == true)
        {
            if(!$sub = $auth_transaction->subscription()) {
                return false;
            }

            $sub->status    = MeprSubscription::$active_str;
            $sub->gateway   = $this->settings->id;
            $sub->store();
            $sub->limit_payment_cycles();
          
            $txn = $this->insert_transaction($sub, $auth_transaction, MeprTransaction::$complete_str);

            $upgrade   = $sub->is_upgrade();
            $downgrade = $sub->is_downgrade();  
            $event_txn = $sub->maybe_cancel_old_sub();
  
            if($upgrade) {
                $this->upgraded_sub($sub, $event_txn);
            }
            elseif($downgrade) {
                $this->downgraded_sub($sub, $event_txn);
            }
            else {
                $this->new_sub($sub, true);
            }

            MeprUtils::send_transaction_receipt_notices($txn);
            MeprUtils::send_signup_notices($txn);

            $sub_id = $sub->subscr_id;
            wp_schedule_single_event($sub->get_expires_at(), 'subscription_time_out_hook', array($sub_id));
            return array('subscription' => $sub, 'transaction' => $txn); 
        }
        
        $auth_transaction->status = MeprTransaction::$failed_str;
        $auth_transaction->store();
        return false;      
    }     


    private function insert_transaction($sub, $transaction, $status) {
        $first_txn = $sub->first_txn();
        if($first_txn == false || !($first_txn instanceof MeprTransaction)) {
          $coupon_id = $sub->coupon_id;
        }
        else {
          $coupon_id = $first_txn->coupon_id;
        }
    
        $old_total = $transaction->total; // Save for later

        $transaction->user_id = $sub->user_id;
        $transaction->product_id = $sub->product_id;
        $transaction->coupon_id = $coupon_id;
        $transaction->txn_type = MeprTransaction::$payment_str;
        $transaction->status = $status;
        $transaction->subscription_id = $sub->id;
        $transaction->trans_num = $transaction->trans_num;
        $transaction->gateway = $this->settings->id;
        $transaction->set_gross($old_total);

        if(!$sub->trial || ($sub->trial and $sub->trial_amount > 0.00)) {
            $expires_at = ($sub->trial) ? MeprUtils::ts_to_mysql_date(time() + MeprUtils::days($sub->trial_days), 'Y-m-d 23:59:59') :  $transaction->expires_at;
            $transaction->expires_at  = $expires_at;
        }else{
            $transaction->expires_at = $transaction->expires_at;
        }

        $transaction->store();    
        return $transaction;
      }


    public function process_update_subscription($subscription_id)
    {
        // Nothing here yet
    }

    /** This method should be used by the class to record a successful cancellation
        * from the gateway. This method should also be used by any IPN requests or
        * Silent Posts.
        */
    public function record_update_subscription()
    {
        // Nothing here yet
    }

    /** Used to suspend a subscription by the given gateway.
        */
    public function process_suspend_subscription($subscription_id)
    {
        $sub = new MeprSubscription($subscription_id);

        if($sub->status == MeprSubscription::$suspended_str) {
          throw new MeprGatewayException(__('This subscription has already been paused.', 'memberpress'));
        }
    
        if($sub->in_free_trial()) {
          throw new MeprGatewayException(__('Sorry, subscriptions cannot be paused during a free trial.', 'memberpress'));
        }
        
        $_REQUEST['recurring_payment_id'] = $sub->subscr_id;
        $this->record_suspend_subscription();
    }

    /** This method should be used by the class to record a successful suspension
        * from the gateway.
        */
    public function record_suspend_subscription()
    {
        $subscr_id = $_REQUEST['recurring_payment_id'];
        $sub = MeprSubscription::get_one_by_subscr_id($subscr_id);
    
        if(!$sub) { return false; }
    
        // Seriously ... if sub was already suspended what are we doing here?
        if($sub->status == MeprSubscription::$suspended_str) { return $sub; }
    
        $sub->status = MeprSubscription::$suspended_str;
        $sub->store();
        
        wp_clear_scheduled_hook('subscription_time_out_hook', array($sub->subscr_id));
        MeprUtils::send_suspended_sub_notices($sub);
    
        return $sub;
    }

    /** Used to suspend a subscription by the given gateway.
        */
    public function process_resume_subscription($subscription_id)
    {
        $sub = new MeprSubscription($subscription_id);    
        $_REQUEST['recurring_payment_id'] = $sub->subscr_id;
        $this->record_resume_subscription();
    }

    /** This method should be used by the class to record a successful resuming of
        * as subscription from the gateway.
        */
    public function record_resume_subscription()
    {
        $subscr_id = $_REQUEST['recurring_payment_id'];
        $sub = MeprSubscription::get_one_by_subscr_id($subscr_id);
    
        if(!$sub) { return false; }
    
        if($sub->status == MeprSubscription::$active_str) { return $sub; }
    
        $sub->status = MeprSubscription::$active_str;
        $sub->store();
        $sub_id = $sub->subscr_id;
    
        //Check if prior txn is expired yet or not, if so create a temporary txn so the user can access the content immediately
        $prior_txn = $sub->latest_txn();
        if($prior_txn == false || !($prior_txn instanceof MeprTransaction) || strtotime($prior_txn->expires_at) < time()) {
            wp_schedule_single_event(time(), 'subscription_time_out_hook', array($sub_id));
        }else{
            wp_schedule_single_event($sub->get_expires_at(), 'subscription_time_out_hook', array($sub_id));
        }
    
        MeprUtils::send_resumed_sub_notices($sub);
        return $sub;
    }

    /** Used to cancel a subscription by the given gateway. This method should be used
        * by the class to record a successful cancellation from the gateway. This method
        * should also be used by any IPN requests or Silent Posts.
        */
    public function process_cancel_subscription($subscription_id)
    {
        $sub = new MeprSubscription($subscription_id);

        $_REQUEST['subscr_id'] = $sub->subscr_id;
        $this->record_cancel_subscription();
      
    }

    /** This method should be used by the class to record a successful cancellation
        * from the gateway. This method should also be used by any IPN requests or
        * Silent Posts.
        */
    public function record_cancel_subscription()
    {
        if(!isset($_REQUEST['subscr_id'])) { return false; }

        $subscr_id = $_REQUEST['subscr_id'];
        $sub = MeprSubscription::get_one_by_subscr_id($subscr_id);
    
        if(!$sub) { return false; }
    
        // Seriously ... if sub was already cancelled what are we doing here?
        if($sub->status == MeprSubscription::$cancelled_str) { return $sub; }
    
        $sub->status = MeprSubscription::$cancelled_str;
        $sub->store();
    

        wp_clear_scheduled_hook('subscription_time_out_hook', array($sub->subscr_id));
        MeprUtils::send_cancelled_sub_notices($sub);
        return $sub;
    }

    /** Gets called when the signup form is posted used for running any payment
        * method specific actions when processing the customer signup form.
        */
    public function process_signup_form($txn)
    {
        // Nothing here yet
    }


    public function spc_payment_fields() {

        if(MeprUtils :: is_product_page()){
            $current_post = MeprUtils::get_current_post();
            $product = new MeprProduct($current_post->ID);
            $mepr_coupon_code = 0;

            if(isset($_GET['coupon']) && !empty($_GET['coupon'])) {
                if(MeprCoupon::is_valid_coupon_code($_GET['coupon'], $product->ID)) {
                    $mepr_coupon_code = htmlentities( sanitize_text_field( $_GET['coupon'] ) );
                }
            }
            
            $payment_required = MeprHooks::apply_filters('mepr_signup_payment_required', $product->adjusted_price($mepr_coupon_code) > 0.00 ? true : false, $product);
        }

        $mepr_options = MeprOptions::fetch();
        $payment_method = $this;
        $payment_form_action = 'mepr-affirm-payment-form';
        $usr = MeprUtils::is_user_logged_in() ? MeprUtils::get_currentuserinfo() : null;
        $payment_required = null;
        return MeprView::get_string("/views/payment_form", get_defined_vars(), array(MP_AFFIRM_PATH));
    }
    

    /** Gets called on the 'init' action after before the payment page is
        * displayed. If we're using an offsite payment solution like PayPal
        * then this method will just redirect to it.
        */
    public function display_payment_page($txn)
    {
        // Nothing here yet
    }

    /** This gets called on wp_enqueue_script and enqueues a set of
        * scripts for use on the page containing the payment form
        */
    public function enqueue_payment_form_scripts()
    {
        $script_url = $this->is_test_mode() ? MeprAffirmAPI::AFFIRM_SANDBOX_SCRIPT_URI : MeprAffirmAPI::AFFIRM_LIVE_SCRIPT_URI;
        $api_key = $this->settings->public_key;

        ?>
            <script>
                _affirm_config = {
                public_api_key:  "<?php echo( $api_key); ?>",
                script:          "<?php echo( $script_url); ?>"
                };
                (function(l,g,m,e,a,f,b){var d,c=l[m]||{},h=document.createElement(f),n=document.getElementsByTagName(f)[0],k=function(a,b,c){return function(){a[b]._.push([c,arguments])}};c[e]=k(c,e,"set");d=c[e];c[a]={};c[a]._=[];d._=[];c[a][b]=k(c,a,b);a=0;for(b="set add save post open empty reset on off trigger ready setProduct".split(" ");a<b.length;a++)d[b[a]]=k(c,e,b[a]);a=0;for(b=["get","token","url","items"];a<b.length;a++)d[b[a]]=function(){};h.async=!0;h.src=g[f];n.parentNode.insertBefore(h,n);delete g[f];d(g);l[m]=c})(window,_affirm_config,"affirm","checkout","ui","script","ready");
            </script>
        <?php

        wp_enqueue_style('mp-affirm-css', MP_AFFIRM_CSS_URL . '/affirm.css');
        wp_enqueue_script('mepr-gateway-checkout', MEPR_JS_URL . '/gateway/checkout.js', array('mepr-checkout-js'), MEPR_VERSION);
        wp_enqueue_script('mepr-affirm-public-js', MP_AFFIRM_JS_URL . '/public.js', array('jquery'), MEPR_VERSION);

    }

    /** This spits out html for the payment form on the registration / payment
        * page for the user to fill out for payment.
        */
    public function display_payment_form($amount, $usr, $product_id, $transaction_id)
    {
        $mepr_options = MeprOptions::fetch();
        $prd = new MeprProduct($product_id);
        $coupon = false;

        $txn = new MeprTransaction($transaction_id);

        //Artifically set the price of the $prd in case a coupon was used
        if($prd->price != $amount) {
            $coupon = true;
            $prd->price = $amount;
        }


        $invoice = MeprTransactionsHelper::get_invoice($txn);
        $billing_type = $prd->is_one_time_payment() ? 'one_time' : 'recurring';

        echo $invoice;
        ?>
            <div class="mp_wrapper mp_payment_form_wrapper">
                <?php MeprView::render('/shared/errors', get_defined_vars()); ?>
                <form action="" method="post" id="mepr_affirm_net_payment_form" class="mepr-checkout-form mepr-form mepr-card-form" novalidate>
                <input type="hidden" name="mepr_process_payment_form" value="Y" />
                <input type="hidden" name="mepr_transaction_id" value="<?php echo $transaction_id; ?>" />
                <input type="hidden" name="mepr_billing_type" value="<?php echo $billing_type; ?>" />
            
                <?php if( empty($usr->first_name) or empty($usr->last_name) ): ?>
                <div class="mp-form-row">
                    <label><?php _e('First Name', 'memberpress'); ?></label>
                    <input type="text" name="user_first_name" class="mepr-form-input" value="<?php echo (isset($_POST['user_first_name'])) ? esc_attr($_POST['user_first_name']) : $usr->first_name; ?>" />
                </div>

                <div class="mp-form-row">
                    <label><?php _e('Last Name', 'memberpress'); ?></label>
                    <input type="text" name="user_last_name" class="mepr-form-input" value="<?php echo (isset($_POST['user_last_name'])) ? esc_attr($_POST['user_last_name']) : $usr->last_name; ?>" />
                </div>
                <?php else: ?>
                <div class="mp-form-row">
                    <input type="hidden" name="user_first_name" value="<?php echo $usr->first_name; ?>" />
                    <input type="hidden" name="user_last_name" value="<?php echo $usr->last_name; ?>" />
                </div>
                <?php endif; ?>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('Adress', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid Adress', 'memberpress'); ?></span>
                    </div>
                    <input type="text" class="mepr-form-input mepr-adress validation"  name="mepr_adress" required />
                </div>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('City', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid City', 'memberpress'); ?></span>
                    </div>
                    <input type="text" class="mepr-form-input mepr-city validation"  name="mepr_city" required />
                </div>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('State', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid State', 'memberpress'); ?></span>
                    </div>
                    <input type="text" class="mepr-form-input mepr-state validation" name="mepr_state"  required />
                </div>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('Zip code for Card', 'memberpress'); ?></label>
                    </div>
                    <input type="text" name="mepr_zip_post_code" class="mepr-form-input validation" autocomplete="off" value="" required />
                </div>
                
                <?php if(isset($usr->user_email)): ?>
                    <input type="hidden" name="mepr_user_email" value="<?php echo $usr->user_email; ?>" />
                <?php else: ?>
                    <div class="mp-form-row">
                        <div class="mp-form-label">
                            <label><?php _e('Email', 'memberpress'); ?></label>
                        </div>
                        <input type="text" name="mepr_user_email" class="mepr-form-input validation" value="" required />
                    </div>
                <?php endif; ?>

                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('Phone', 'memberpress'); ?></label>
                    </div>
                    <input type="text" name="mepr_user_phone" class="mepr-form-input validation" value="" required />
                </div>


                <?php if(!$prd->is_one_time_payment()): ?>
                    <div class="stripe-additional">
                            <div class="mp-form-row">
                                <div class="mp-form-label">
                                    <label><?php _e('Credit Card Number', 'memberpress'); ?></label>
                                    <span class="cc-error"><?php _e('Invalid Credit Card Number', 'memberpress'); ?></span>
                                </div>
                                <input type="tel" name="mepr_cc_num"  class="mepr-form-input cc-number validation" required pattern="\d*" autocomplete="cc-number" />
                            </div>
                            <div class="mp-form-row">
                                <div class="mp-form-label">
                                    <label><?php _e('Expiration', 'memberpress'); ?></label>
                                    <span class="cc-error"><?php _e('Invalid Expiration', 'memberpress'); ?></span>
                                </div>
                                <input type="tel" name="mepr_cc_exp" class="mepr-form-input cc-exp validation" required pattern="\d*" autocomplete="cc-exp" placeholder="mm/yy" />
                            </div>
                            <div class="mp-form-row">
                                <div class="mp-form-label">
                                    <label><?php _e('CVC', 'memberpress'); ?></label>
                                    <span class="cc-error"><?php _e('Invalid CVC Code', 'memberpress'); ?></span>
                                </div>
                                <input type="tel" name="mepr_cvv_code" class="mepr-form-input card-cvc cc-cvc validation" required pattern="\d*" autocomplete="off" />
                            </div>
                        </div>
                <?php endif; ?>

                <?php if(isset($prd) && $prd->price > 100): ?>
                <p id="learn-more" class="affirm-as-low-as" data-amount="<?php echo MeprUtils::format_float($prd->price * 100, 0) ?>" data-affirm-color="blue" data-learnmore-show="true" data-page-type="product"></p>
                <?php endif; ?>

                <div class="mepr_spacer">&nbsp;</div>

                <input type="submit" class="mepr-submit" value="<?php _e('Submit', 'memberpress'); ?>" />
                <img src="<?php echo admin_url('images/loading.gif'); ?>" alt="<?php _e('Loading...', 'memberpress'); ?>" style="display: none;" class="mepr-loading-gif" />
                <?php MeprView::render('/shared/has_errors', get_defined_vars()); ?>
            </form>
            </div>
        <?php

        MeprHooks::do_action('mepr-affirm-payment-form', $txn);
    }


    /** Validates the payment form before a payment is processed */
    public function validate_payment_form($errors)
    {
        $mepr_options = MeprOptions::fetch();
        
        if(!isset($_POST['mepr_transaction_id']) || !is_numeric($_POST['mepr_transaction_id'])) {
            $errors[] = __('An unknown error has occurred.', 'memberpress');
          }

        // IF SPC is enabled, we need to bail on validation if 100% off forever coupon was used
        $txn = new MeprTransaction((int)$_POST['mepr_transaction_id']);
        if($txn->coupon_id) {
            $coupon = new MeprCoupon($txn->coupon_id);
            if($coupon->discount_amount == 100 && $coupon->discount_type == 'percent' && ($coupon->discount_mode == 'standard' || $coupon->discount_mode == 'trial-override' || $coupon->discount_mode == 'first-payment')) {
              return $errors;
            }
          }
      
        if(!$mepr_options->show_fname_lname ||
            (!isset($_POST['user_first_name']) || empty($_POST['user_first_name']) ||
            !isset($_POST['user_last_name']) || empty($_POST['user_last_name']))) {
            $errors[] = __('Your first name and last name must not be blank.', 'memberpress');
        }

        if(!isset($_POST['mepr_adress']) || empty($_POST['mepr_adress'])) {
            $errors[] = __('You must enter your Adress.', 'memberpress');
        }

        if(!isset($_POST['mepr_city']) || empty($_POST['mepr_city'])) {
            $errors[] = __('You must enter your City.', 'memberpress');
        }

        if(!isset($_POST['mepr_state']) || empty($_POST['mepr_state'])) {
            $errors[] = __('You must enter your State.', 'memberpress');
        }

        if(!isset($_POST['mepr_zip_post_code']) || empty($_POST['mepr_zip_post_code'])){
            $errors[] = __('You must enter your Zip.', 'memberpress');
        }

        if((!isset($_POST['mepr_user_email']) || empty($_POST['mepr_user_email'])) && (!isset($_POST['user_email']) || empty($_POST['user_email']))){
            $errors[] = __('You must enter your Email.', 'memberpress');
        }

        if(!isset($_POST['mepr_user_phone']) || empty($_POST['mepr_user_phone'])){
            $errors[] = __('You must enter your Phone.', 'memberpress');
        }

        return $errors;
    }


    /** 
     * Displays the form for the given payment gateway on the MemberPress Options page 
    */
    public function display_options_form()
    {
        $mepr_options = MeprOptions::fetch();

        $test_secret_key      = trim($this->settings->api_keys['test']['secret']);
        $test_public_key      = trim($this->settings->api_keys['test']['public']);
        $live_secret_key      = trim($this->settings->api_keys['live']['secret']);
        $live_public_key      = trim($this->settings->api_keys['live']['public']);
        $test_mode            = ($this->settings->test_mode == 'on' or $this->settings->test_mode == true);
    
        $test_secret_key_str      = "{$mepr_options->integrations_str}[{$this->id}][api_keys][test][secret]";
        $test_public_key_str      = "{$mepr_options->integrations_str}[{$this->id}][api_keys][test][public]";
        $live_secret_key_str      = "{$mepr_options->integrations_str}[{$this->id}][api_keys][live][secret]";
        $live_public_key_str      = "{$mepr_options->integrations_str}[{$this->id}][api_keys][live][public]";
        $test_mode_str            = "{$mepr_options->integrations_str}[{$this->id}][test_mode]"; 
    
    ?>
        <table>
            <tr>
                <td><?php _e('Live Public Key*:', 'memberpress'); ?></td>
                <td><input type="text" class="mepr-auto-trim" name="<?php echo $live_public_key_str; ?>" value="<?php echo $live_public_key; ?>"/></td>
            </tr>
            <tr>
                <td><?php _e('Live Private Key*:', 'memberpress'); ?></td>
                <td><input type="text" class="mepr-auto-trim" name="<?php echo $live_secret_key_str; ?>" value="<?php echo $live_secret_key; ?>" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox" name="<?php echo $test_mode_str; ?>" id="test-mode-<?php echo $this->id; ?>" class="test_mode_checkbox" data-value="<?php echo $this->id;?>" <?php echo checked($test_mode); ?> />
                    <label for="test-mode-<?php echo $this->id;?>"><?php _e('Test Mode', 'memberpress'); ?></label>
                </td>
            </tr>
            <tr class="test_mode_row-<?php echo $this->id;?> mepr_hidden">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em><?php _e('Test Public Key*:', 'memberpress'); ?></em></td>
                <td><input type="text" class="mepr-auto-trim" name="<?php echo $test_public_key_str; ?>" value="<?php echo $test_public_key; ?>"/></td>
            </tr>
            <tr class="test_mode_row-<?php echo $this->id;?> mepr_hidden">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em><?php _e('Test Private Key*:', 'memberpress'); ?></em></td>
                <td><input type="text" class="mepr-auto-trim" name="<?php echo $test_secret_key_str; ?>" value="<?php echo $test_secret_key; ?>" /></td>
            </tr>
            <?php MeprHooks::do_action('mepr-affirm-options-form', $this); ?>
        </table>
    <?php
    }

    /** Validates the form for the given payment gateway on the MemberPress Options page */
    public function validate_options_form($errors)
    {
        $mepr_options = MeprOptions::fetch();

        $test_mode            = ($this->settings->test_mode == 'on' or $this->settings->test_mode == true);    
        if (
            !isset($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['live']['secret']) ||
            empty($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['live']['secret'])  ||
            !isset($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['live']['public']) ||
            empty($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['live']['public'])
          ) {
            $errors[] = __("All Affirm live keys must be filled in.", 'memberpress');
          }

        if($test_mode)
        {
            if (
                !isset($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['test']['secret']) ||
                empty($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['test']['secret'])  ||
                !isset($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['test']['public']) ||
                empty($_REQUEST[$mepr_options->integrations_str][$this->id]['api_keys']['test']['public'])
              ) {
                $errors[] = __("All Affirm test keys in Test Mode must be filled in.", 'memberpress');
              }
        }

        return $errors;
    }

    /** Displays the update account form on the subscription account page **/
    public function display_update_account_form($subscription_id, $errors=array(), $message="")
    {
        ?>
        <h3><?php _e('Updating your Affirm Account Information', 'memberpress'); ?></h3>
        <div><?php printf(__('To update your Affirm Account Information, please go to %sAffirm.com%s, login and edit your account information there.', 'memberpress'), '<a href="https://www.affirm.com/" target="blank">', '</a>'); ?></div>
        <?php
    }

    /** Validates the payment form before a payment is processed */
    public function validate_update_account_form($errors=array())
    {
    // We'll have them update their cc info on affirm.com
    }

    /** Actually pushes the account update to the payment processor */
    public function process_update_account_form($subscription_id)
    {
    // We'll have them update their cc info on affirm.com
    }

    /** Returns boolean ... whether or not we should be sending in test mode or not */
    public function is_test_mode()
    {
        return (isset($this->settings->test_mode) and $this->settings->test_mode);
    }

    /** Returns boolean ... whether or not we should be forcing ssl */
    public function force_ssl() 
    {
        return false; 
    }


}

?>