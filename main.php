<?php

/**
 * Plugin Name: MemberPress Affirm
 * Description: Affirm integration for MemberPress.
 * Version: 1.0.0
 * Developer: ZelloCoding
 */

if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (is_plugin_active('memberpress/memberpress.php')) {
    
    define('MP_AFFIRM_PLUGIN_SLUG', 'affirm-memberpress/main.php');
    define('MP_AFFIRM_PLUGIN_NAME', 'affirm-memberpress');
    define('MP_AFFIRM_EDITION', MP_AFFIRM_PLUGIN_NAME);
    define('MP_AFFIRM_PATH', WP_PLUGIN_DIR . '/' . MP_AFFIRM_PLUGIN_NAME);

    $mp_url_protocol = (is_ssl()) ? 'https' : 'http'; // Make all of our URLS protocol agnostic
    define('MP_AFFIRM_URL', preg_replace('/^https?:/', "{$mp_url_protocol}:", plugins_url('/' . MP_AFFIRM_PLUGIN_NAME)));
    define('MP_AFFIRM_CSS_URL', MP_AFFIRM_URL . '/css');
    define('MP_AFFIRM_JS_URL', MP_AFFIRM_URL . '/js');
    define('MP_AFFIRM_IMAGES_URL', MP_AFFIRM_URL . '/images');

    // Load Memberpress Base Gateway
    require_once(MP_AFFIRM_PATH . '/../memberpress/app/lib/MeprBaseGateway.php');
    require_once(MP_AFFIRM_PATH . '/../memberpress/app/lib/MeprBaseRealGateway.php');

    // Load Memberpress Paystack API
    require_once(MP_AFFIRM_PATH . '/MeprAffirmAPI.php');

    // Load Memberpress Paystack Addon
    require_once(MP_AFFIRM_PATH . '/MeprAffirm.php');

    // Load Stripe
    require_once(MP_AFFIRM_PATH . '/libs/Stripe/init.php');

   
    new MeprAffirm;
}
?>