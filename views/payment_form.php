<?php
    if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.'); }
    if(!isset($product) || empty($product->ID))
    {
        $pattern = '/\[mepr-membership-registration-form\sid="(\d+)"]/';
        $post = MeprUtils::get_current_post();
        $content =   $post -> post_content;
        if(preg_match($pattern, $content, $matches))
        {
            $product = new MeprProduct($matches[1]);
            $mepr_coupon_code = 0;

            if(isset($_GET['coupon']) && !empty($_GET['coupon'])) {
                if(MeprCoupon::is_valid_coupon_code($_GET['coupon'], $product->ID)) {
                    $mepr_coupon_code = htmlentities( sanitize_text_field( $_GET['coupon'] ) );
                }
            }
            
            $payment_required = MeprHooks::apply_filters('mepr_signup_payment_required', $product->adjusted_price($mepr_coupon_code) > 0.00 ? true : false, $product);
        }
        else{
            return;
        }
    }
?>

<div class="mepr-checkout-form sps">
    <div class="mp-form-row">
        <div class="mp-form-label">
            <label><?php _e('Address', 'memberpress'); ?></label>
        </div>
        <input type="text" class="mepr-form-input mepr-adress validation"  name="mepr_adress" required />
    </div>
    <div class="mp-form-row">
        <div class="mp-form-label">
            <label><?php _e('City', 'memberpress'); ?></label>
        </div>
        <input type="text" class="mepr-form-input mepr-city validation"  name="mepr_city" required />
    </div>
    <div class="mp-form-row">
        <div class="mp-form-label">
            <label><?php _e('State', 'memberpress'); ?></label>
        </div>
        <input type="text" class="mepr-form-input mepr-state validation" name="mepr_state" placeholder="CA"  required />
    </div>
    <div class="mp-form-row">
  <div class="mp-form-label">
        <label><?php _e('Zip code', 'memberpress'); ?></label>
        </div>
        <input type="text" name="mepr_zip_post_code" class="mepr-form-input" autocomplete="off" value="" required />
    </div>
    <?php if(isset($usr->user_email)): ?>
        <input type="hidden" name="mepr_user_email" value="<?php echo $usr->user_email; ?>" />
    <?php else: ?>
    <div class="mp-form-row">
        <div class="mp-form-label">
            <label><?php _e('Email', 'memberpress'); ?></label>
        </div>
        <input type="text" name="mepr_user_email" class="mepr-form-input validation" value="" required />
    </div>
    <?php endif; ?>
        <div class="mp-form-row">
            <div class="mp-form-label">
                <label><?php _e('Phone', 'memberpress'); ?></label>
            </div>
            <input type="text" name="mepr_user_phone" class="mepr-form-input validation" value="" required />
        </div>
    <?php if(!$product->is_one_time_payment()): ?>
        <div class="stripe-additional">
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('Credit Card Number', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid Credit Card Number', 'memberpress'); ?></span>
                    </div>
                    <input type="tel" name="mepr_cc_num" class="mepr-form-input cc-number validation"  required  pattern="\d*" autocomplete="cc-number"  />
                </div>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('Expiration', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid Expiration', 'memberpress'); ?></span>
                    </div>
                    <input type="tel" name="mepr_cc_exp" class="mepr-form-input cc-exp validation " required pattern="\d*" autocomplete="cc-exp" placeholder="mm/yy" />
                </div>
                <div class="mp-form-row">
                    <div class="mp-form-label">
                        <label><?php _e('CVC', 'memberpress'); ?></label>
                        <span class="cc-error"><?php _e('Invalid CVC Code', 'memberpress'); ?></span>
                    </div>
                    <input type="tel" name="mepr_cvv_code" class="mepr-form-input card-cvc cc-cvc validation" required pattern="\d*" autocomplete="off" />
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(isset($product) && $product->price > 100): ?>
    <p id="learn-more" class="affirm-as-low-as" data-amount="<?php echo MeprUtils::format_float($product->price * 100, 0) ?>" data-affirm-color="blue" data-learnmore-show="true" data-page-type="product"></p>
    <?php endif; ?>
</div>

<noscript>
    <p class="mepr_nojs"><?php _e('Javascript is disabled in your browser. You will not be able to complete your purchase until you either enable JavaScript in your browser, or switch to a browser that supports it.', 'memberpress'); ?></p>
</noscript>
