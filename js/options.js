
jQuery(document).ready(function ($) {
    
    //Affirm Test mode show inputs
    $('.test_mode_checkbox').each(function() {
        if($(this).is(':checked')) {
            var id = $(this).attr('data-value');
            $('.test_mode_row-' + id).show();
        }
    });

    $('body').on('click', '.test_mode_checkbox', function(e) {
        var id = $(this).attr('data-value');
        $('.test_mode_row-' + id).toggle();
      });

});
