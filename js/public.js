
jQuery(document).ready(function ($) {
    
    jQuery('body').on('click', '.mepr-signup-form div[class^=mepr-payment-method] input.mepr-form-radio', function (e) {

        var form = jQuery(this).closest('.mepr-signup-form');
        // Reset the transaction ID to prevent any issues after switching payment methods
        form.find('input[name="mepr_transaction_id"]').val('');

        var pmid = '.mp-pm-desc-' + jQuery(this).val();
        var pmid_exists = (form.find(pmid).length > 0);
        form.find('.mepr-payment-method-desc-text').addClass('mepr-close');

        if(pmid_exists) {
            form.find(pmid).removeClass('mepr-close');
        }

        //If nothing has the mepr-close class, we still need to show this one's description
        var mepr_close_exists = (form.find('.mepr-payment-method-desc-text.mepr-close').length > 0);
        if(mepr_close_exists) {

            form.find('.mepr-payment-method-desc-text.mepr-close').css('display', 'none');
            if(pmid_exists) {
                form.find(pmid).css('display', 'block');
            }
            
        } else {
            if(pmid_exists) {
                form.find(pmid).css('display', 'block');
            }
        }
    });
    
});
