<?php
if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}
/*
Integration of Paystack into MemberPress
*/
class MeprAffirm
{
    public function __construct()
    {
        // Add Gateway Path
        add_filter('mepr-gateway-paths', array($this, 'add_mepr_gateway_paths'));

        // Add Option Scripts
        add_action('mepr-options-admin-enqueue-script', array($this, 'add_options_admin_enqueue_script'));
        
        add_action('subscription_time_out_hook', array($this, 'update_subscription_payment'));
    }

    //Add Paystack path to general gateway page
    public function add_mepr_gateway_paths($tabs)
    {
        array_push($tabs, MP_AFFIRM_PATH);
        return $tabs;
    }

    public function add_options_admin_enqueue_script($hook)
    {
        if ($hook == 'memberpress_page_memberpress-options') {
            wp_enqueue_script(
                'mp-affirm-options-js',
                MP_AFFIRM_JS_URL . '/options.js',
                array(
                    'jquery',
                )
            );
            return $hook;
        }
    }

      // Update sub payment after time out
      public function update_subscription_payment($sub_id)
      {
        $sub = MeprSubscription::get_one_by_subscr_id($sub_id);
        $sub->status = MeprSubscription::$pending_str;
        $sub->store();

        if(empty($sub) || !($sub instanceof MeprSubscription))
            return ;
  
        $user = $sub->user();
        $last_txn = $sub->latest_txn();
        $product = $last_txn->product();
          
        if(empty($user) || empty($last_txn) || empty($product))
            return;
  
        $mepr_options = MeprOptions::fetch();
        foreach ( $mepr_options->integrations as $integration ) {
            if ( $integration['gateway'] == 'MeprStripeGateway' ) {
                $payment_method = new MeprStripeGateway();
                $payment_method->load( $integration );
                break;
            }
        }            
    
        if(empty($payment_method) || !($payment_method instanceof MeprStripeGateway))
            return;


        $sub->gateway = $payment_method->id;
        if($sub->status == MeprSubscription::$cancelled_str){
            $sub->store();
            return;
        }
  
        $sub->status = MeprSubscription::$pending_str;
        $sub->store();
  
        $cc_num = get_user_meta($user->ID, 'mepr_cc_num', true);
        $cc_exp = get_user_meta($user->ID, 'mepr_cc_exp', true);
        $cvv_code = get_user_meta($user->ID, 'mepr_cvv_code', true);

        if(empty($cc_num) || empty($cc_exp) || empty($cvv_code))
            return;
  
        $num = str_replace(" ", '', $cc_num);
        $exp = explode('/', $cc_exp);
        $exp_month = $exp[0];
        $exp_year = strlen($exp[1]) > 2 ? substr($exp[1], -2):  $exp[1];


        $stripe = new \Stripe\StripeClient(
           $payment_method->settings->secret_key
        );
        
        $txn = new MeprTransaction();
        $txn->product_id = $product->ID;
        $txn->user_id = $user->ID;
        $txn->set_subtotal($product->price);
        $txn->gateway    = $payment_method->id;
        $txn->subscription_id = $sub->id;
        $txn->store();

        try{
            $token = $stripe->tokens->create([
                'card' => [
                  'number' => intval($num),
                  'exp_month' => intval($exp_month),
                  'exp_year' => intval($exp_year),
                  'cvc' => intval($cvv_code),
                ],
            ]);
    
            $_REQUEST['stripeToken'] = $token['id'];
            $result = $payment_method->process_create_subscription($txn);
        }catch(Exeption $e){
            $txn->status = MeprTransaction::$failed_str;
            $txn->store();
        }


      }
  

}
